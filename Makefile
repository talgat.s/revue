# dev
review_host_dev=reviewhost
review_port_dev=8000
product_host_dev=producthost
product_port_dev=8001
db_host_dev=dbhost
db_host_external_dev=localhost
db_port_dev=5432
db_name_dev=revue
db_user_dev=revuesuperuser
db_password_dev=vLxZa2bgQK

# test
review_host_test=reviewhost
review_port_test=4001
product_host_test=producthost
product_port_test=8001
db_host_test=dbhost
db_host_external_test=localhost
db_port_test=5432
db_name_test=revue_test
db_user_test=revuesuperuser
db_password_test=t3f8kJ8QnR

.PHONY: compose-up
compose-up: deployment/compose/docker-compose.yaml
	REVIEW_HOST=$(review_host_dev) \
	REVIEW_PORT=$(review_port_dev) \
	PRODUCT_HOST=$(product_host_dev) \
	PRODUCT_PORT=$(product_port_dev) \
	DB_HOST=$(db_host_dev) \
	DB_PORT=$(db_port_dev) \
	DB_NAME=$(db_name_dev) \
	DB_USER=$(db_user_dev) \
	DB_PASSWORD=$(db_password_dev) \
	docker compose -f ./deployment/compose/docker-compose.yaml up

.PHONY: compose-down
compose-down: deployment/compose/docker-compose.yaml
	REVIEW_HOST=$(review_host_dev) \
	REVIEW_PORT=$(review_port_dev) \
	PRODUCT_HOST=$(product_host_dev) \
	PRODUCT_PORT=$(product_port_dev) \
	DB_HOST=$(db_host_dev) \
	DB_PORT=$(db_port_dev) \
	DB_NAME=$(db_name_dev) \
	DB_USER=$(db_user_dev) \
	DB_PASSWORD=$(db_password_dev) \
	docker compose -f ./deployment/compose/docker-compose.yaml down

.PHONY: migrate
migrate:
	cd views/review/cmd/db/migrations \
	&& DB_HOST=$(db_host_external_dev) DB_PORT=$(db_port_dev) DB_NAME=$(db_name_dev) DB_USER=$(db_user_dev) DB_PASSWORD=$(db_password_dev) go run ../../../bin/bun-cli.go db init \
	&& DB_HOST=$(db_host_external_dev) DB_PORT=$(db_port_dev) DB_NAME=$(db_name_dev) DB_USER=$(db_user_dev) DB_PASSWORD=$(db_password_dev) go run ../../../bin/bun-cli.go db migrate

.PHONY: rollback
rollback:
	cd views/review/cmd/db/migrations \
	&& DB_HOST=$(db_host_external_dev) DB_PORT=$(db_port_dev) DB_NAME=$(db_name_dev) DB_USER=$(db_user_dev) DB_PASSWORD=$(db_password_dev) go run ../../../bin/bun-cli.go db rollback

.PHONY: seed
seed:
	cd views/review/cmd/db/seed \
	&& DB_HOST=$(db_host_external_dev) DB_PORT=$(db_port_dev) DB_NAME=$(db_name_dev) DB_USER=$(db_user_dev) DB_PASSWORD=$(db_password_dev) go run ../../../bin/bun-cli.go db seed

.PHONY: bun-cli
bun-cli:
	cd views/review/cmd/db/migrations \
	&& DB_HOST=$(db_host_external_dev) DB_PORT=$(db_port_dev) DB_NAME=$(db_name_dev) DB_USER=$(db_user_dev) DB_PASSWORD=$(db_password_dev) go run ../../../bin/bun-cli.go $(filter-out $@,$(MAKECMDGOALS))

.PHONY: test/network/create
test/network/create:
	docker network create db

.PHONY: test/network/remove
test/network/remove:
	docker network rm db

.PHONY: test/image/run/db
test/image/run/db:
	docker pull "postgres:14-alpine"
	docker run --rm -itd \
		--name db \
		--network db \
		-e POSTGRES_DB=$(db_name_test) \
		-e POSTGRES_USER=$(db_user_test) \
		-e POSTGRES_PASSWORD=$(db_password_test) \
		-p $(db_port_test):$(db_port_test) \
		"postgres:14-alpine"

.PHONY: test/image/run/review
test/image/run/review: views/review/
	docker build -t "revue-review-test:latest" "./views/review"
	docker run --rm -itd \
		--name review \
		--network db \
		-e DB_HOST=$(db_host_test) \
		-e DB_PORT=$(db_port_test) \
		-e DB_NAME=$(db_name_test) \
		-e DB_USER=$(db_user_test) \
		-e DB_PASSWORD=$(db_password_test) \
		-p $(port_test):80 \
		"revue-review-test:latest"

.PHONY: test/image/run
test/image/run:
	$(MAKE) --no-print-directory test/image/run/db
	$(MAKE) --no-print-directory test/image/run/review

.PHONY: test/image/stop/review
test/image/stop/review: views/review/
	docker container stop review

.PHONY: test/image/stop/db
test/image/stop/db:
	docker container stop db

.PHONY: test/image/stop
test/image/stop:
	$(MAKE) --no-print-directory test/image/stop/review
	$(MAKE) --no-print-directory test/image/stop/db

.PHONY: test/migrate
test/migrate: views/review/cmd/db/migrations
	cd views/review/cmd/db/migrations \
	&& DB_HOST=$(db_host_external_test) DB_PORT=$(db_port_test) DB_NAME=$(db_name_test) DB_USER=$(db_user_test) DB_PASSWORD=$(db_password_test) go run ../../../bin/bun-cli.go db init \
	&& DB_HOST=$(db_host_external_test) DB_PORT=$(db_port_test) DB_NAME=$(db_name_test) DB_USER=$(db_user_test) DB_PASSWORD=$(db_password_test) go run ../../../bin/bun-cli.go db migrate

.PHONY: test/rollback
test/rollback:
	cd views/review/cmd/db/migrations \
	&& DB_HOST=$(db_host_external_test) DB_PORT=$(db_port_test) DB_NAME=$(db_name_test) DB_USER=$(db_user_test) DB_PASSWORD=$(db_password_test) go run ../../../bin/bun-cli.go db rollback

.PHONY: test/run/integration
test/run/integration: test/integration/
	cd test/integration/ && go test -v ./...

.PHONY: test
test: test/integration/
	$(MAKE) --no-print-directory test/network/create
	$(MAKE) --no-print-directory test/image/run
	$(MAKE) -i --no-print-directory test/run/integration
	$(MAKE) --no-print-directory test/image/stop
	$(MAKE) --no-print-directory test/network/remove



