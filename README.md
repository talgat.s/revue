# Revue Project

**NOTE:** instead of products used movies because adidas has no public api
&& couldn't accomplish integration tests. But left some work. 

## Getting started

1. Clone project

```bash
git clone git@gitlab.com:talgat.s/revue.git --recurse-submodules
```

2. Run docker compose

```bash
make compose
```

3. Migrate DB

```bash
make migrate
```

4. Seed DB

```bash
make seed
```

## Rollback

Rollback DB

```bash
make rollback
```